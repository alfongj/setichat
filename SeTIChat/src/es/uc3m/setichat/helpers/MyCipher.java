package es.uc3m.setichat.helpers;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import es.uc3m.setichat.utils.Base64;

/**
 * AES encryption algorithm in Cipher Block Chaining (CBC) mode.
 */
public class MyCipher {

	Cipher cipher;

	/**
	 * 
	 * @param uncypher_message
	 *            solo el message que se va a cifrar, todo lo demas se genera
	 *            dentro (NO EN BASE 64)
	 * @param his_public_key
	 *            su clave publica (EN BASE 64)
	 * @return todo el mensaje (EN BASE 64): RSA(AESkey) | IV | AES(message)
	 * @throws Exception
	 */

	public String encrypt_message_AES(String uncypher_message,
			String his_public_key_64) throws Exception {

		// 0. El texto es codificado previamente utilizando codificaci�n
		// �UTF-8�.
		// String message = "This string contains a secret message.";
		byte[] b = uncypher_message.getBytes("UTF-8");
		byte[] his_public_key = Base64.decode(his_public_key_64);

		// 1. Se genera aleatoriamente una clave de AES 128.
		KeyGenerator keygen = KeyGenerator.getInstance("AES");
		keygen.init(128);

		byte[] secretKey = keygen.generateKey().getEncoded();
		SecretKeySpec skeySpec = new SecretKeySpec(secretKey, "AES");

		// 2. Se genera aleatoriamente un vector de inicializaci�n para AES 128.
		IvParameterSpec ivspec = createRandomIV(new SecureRandom());

		// 3. Utilizando la clave sim�trica anterior y el algoritmo de cifrado
		// AES 128 en modo CBC y padding PKCS5
		cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivspec);

		// encrypt the message
		byte[] encrypted = cipher.doFinal(b);

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

		// concat all 3 parts of concat_message
		outputStream.write(encrypt_key_RSA(skeySpec.getEncoded(),
				his_public_key));
		outputStream.write(ivspec.getIV());
		outputStream.write(encrypted);

		byte[] concat_message = outputStream.toByteArray();

		// format base64
		String b64 = Base64.encodeToString(concat_message, false);
		return b64;
	}

	/**
	 * 
	 * @param plain_AES_key
	 *            la clave AES a cifrar, SIN BASE 64
	 * @param his_public_key
	 *            su clave publica, EN BASE 64
	 * @return clave publica AES cifrada con RSA, SIN BASE 64
	 */
	private byte[] encrypt_key_RSA(byte[] plain_AES_key, byte[] his_public_key) {
		byte[] resul = null;
		try {
			Cipher publicKeyCipher = Cipher.getInstance("RSA/ECB/PKCS1PADDING");
			// convertir de String a publicKey
			X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(
					his_public_key);
			KeyFactory keyFact = KeyFactory.getInstance("RSA");
			java.security.interfaces.RSAPublicKey pubKey = (RSAPublicKey) keyFact
					.generatePublic(x509KeySpec);

			// cifrado RSA
			publicKeyCipher.init(Cipher.ENCRYPT_MODE, pubKey);
			resul = publicKeyCipher.doFinal(plain_AES_key);

		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}

		return resul;
	}

	/**
	 * 
	 * @param concat_message
	 *            cadena completa (EN BASE 64) concatenada tal cual se recibe en
	 *            el servidor
	 * @param our_private_key_64
	 *            la clave privada (EN BASE 64)
	 * @param our_public_key_64
	 *            nuestra clave publica (EN BASE 64) que ha usado el otro
	 *            contacto
	 * @return solo el mensaje descifrado que se mostrara en el chat
	 */
	public String decrypt_concat_message_AES(String concat_message,
			String our_private_key_64, String our_public_key_64) {
		byte[] concat_message_bytes = Base64.decode(concat_message);
		byte[] our_private_key = Base64.decode(our_private_key_64);
		byte[] our_public_key = Base64.decode(our_public_key_64);
		byte[] decrypted = null;

		// separa mensaje concatenado

		int tamano_key_cifrada = 0;
		try {
			X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(
					our_public_key);
			KeyFactory keyFact;
			keyFact = KeyFactory.getInstance("RSA");
			java.security.interfaces.RSAPublicKey pubKey = (RSAPublicKey) keyFact
					.generatePublic(x509KeySpec);
			tamano_key_cifrada = pubKey.getModulus().bitLength() / 8;
		} catch (NoSuchAlgorithmException e1) {
			e1.printStackTrace();
		} catch (InvalidKeySpecException e1) {
			e1.printStackTrace();
		}

		// primeros x bytes que corresponden a la clave cifrada
		byte[] key = Arrays.copyOfRange(concat_message_bytes, 0,
				tamano_key_cifrada);
		SecretKeySpec keyspec = new SecretKeySpec(decrypt_key_RSA(key,
				our_private_key), "AES");

		// siguientes 16 bytes
		byte[] iv = Arrays.copyOfRange(concat_message_bytes,
				tamano_key_cifrada, 16 + tamano_key_cifrada);
		IvParameterSpec ivspec = new IvParameterSpec(iv);

		// resto de bytes
		byte[] cyphered_message = Arrays.copyOfRange(concat_message_bytes,
				16 + tamano_key_cifrada, concat_message_bytes.length);

		try {
			cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, keyspec, ivspec);
			decrypted = cipher.doFinal(cyphered_message);

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (InvalidAlgorithmParameterException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}
		try {
			return new String(decrypted, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 
	 * @param encrypted_AES_key
	 *            la clave AES SIN BASE 64
	 * @param our_private_key
	 *            nuestra clave CON BASE 64
	 * @return
	 */
	private byte[] decrypt_key_RSA(byte[] encrypted_AES_key,
			byte[] our_private_key) {
		byte[] resul = null;
		try {
			Cipher privateKeyCipher = Cipher
					.getInstance("RSA/ECB/PKCS1PADDING");
			// convertir de String a privateKey
			PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(
					our_private_key);
			KeyFactory keyFact = KeyFactory.getInstance("RSA");
			PrivateKey priKey = keyFact.generatePrivate(keySpec);
			// descifrado RSA
			privateKeyCipher.init(Cipher.DECRYPT_MODE, priKey);
			resul = privateKeyCipher.doFinal(encrypted_AES_key);
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}
		return resul;
	}

	private static String hexEncode(byte[] input) {
		if (input == null || input.length == 0) {
			return "";
		}

		int inputLength = input.length;
		StringBuilder output = new StringBuilder(inputLength * 2);

		for (int i = 0; i < inputLength; i++) {
			int next = input[i] & 0xff;
			if (next < 0x10) {
				output.append("0");
			}

			output.append(Integer.toHexString(next));
		}

		return output.toString();
	}

	private static IvParameterSpec createRandomIV(SecureRandom random) {
		byte[] ivBytes = new byte[16];
		random.nextBytes(ivBytes);

		return new IvParameterSpec(ivBytes);
	}

	public byte[][] generateRSAKeys() {
		byte[][] keys = new byte[2][];
		KeyPairGenerator keyGen = null;
		try {
			keyGen = KeyPairGenerator.getInstance("RSA");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		keyGen.initialize(1024); // tamano clave 1024 bits
		KeyPair clavesRSA = keyGen.generateKeyPair();
		PrivateKey clavePrivada = clavesRSA.getPrivate();
		PublicKey clavePublica = clavesRSA.getPublic();

		keys[0] = clavePrivada.getEncoded();
		keys[1] = clavePublica.getEncoded();

		return keys;
	}

}
