package es.uc3m.setichat.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import es.uc3m.setichat.R;

/**
 * Activity which displays a login screen to the user, offering registration as
 * well.
 */
public class CreateAccount extends Activity {
	/**
	 * Keep track of the login task to ensure we can cancel it if requested.
	 */
	private CreateAccountTask mCreateAccount = null;

	// constants
	static final int ACTION_SIGN_UP = 0;

	// Values for user and phone number at the time of the login attempt.
	private String mUser;
	private String mPhoneNumber;

	// UI references.
	private EditText mUserView;
	private EditText mPhoneNumberView;
	private View mLoginFormView;
	private View mLoginStatusView;
	private TextView mLoginStatusMessageView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_create_account);

		// Set up the create account form.
		mUserView = (EditText) findViewById(R.id.user);

		mPhoneNumberView = (EditText) findViewById(R.id.phone);
		mPhoneNumberView
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView textView, int id,
							KeyEvent keyEvent) {
						if (id == R.id.login || id == EditorInfo.IME_NULL) {
							attemptCreateAccount();
							return true;
						}
						return false;
					}
				});

		mLoginFormView = findViewById(R.id.login_form);
		mLoginStatusView = findViewById(R.id.login_status);
		mLoginStatusMessageView = (TextView) findViewById(R.id.login_status_message);

		findViewById(R.id.sign_in_button).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						attemptCreateAccount();
					}
				});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.create_account, menu);
		return true;
	}

	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	public void attemptCreateAccount() {
		if (mCreateAccount != null) {
			return;
		}

		// Reset errors.
		mUserView.setError(null);
		mPhoneNumberView.setError(null);

		// Store values at the time of the login attempt.
		mUser = mUserView.getText().toString();
		mPhoneNumber = mPhoneNumberView.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid phone number.
		if (TextUtils.isEmpty(mPhoneNumber)) {
			mPhoneNumberView.setError(getString(R.string.error_field_required));
			focusView = mPhoneNumberView;
			cancel = true;
		} else if (mPhoneNumber.length() < 9) {
			mPhoneNumberView
					.setError(getString(R.string.error_invalid_password));
			focusView = mPhoneNumberView;
			cancel = true;
		}

		// Check for a valid user
		if (TextUtils.isEmpty(mUser)) {
			mUserView.setError(getString(R.string.error_field_required));
			focusView = mUserView;
			cancel = true;
		}

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
			showProgress(true);
			mCreateAccount = new CreateAccountTask();
			mCreateAccount.execute((Void) null);
		}
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mLoginStatusView.setVisibility(View.VISIBLE);
			mLoginStatusView.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginStatusView.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});

			mLoginFormView.setVisibility(View.VISIBLE);
			mLoginFormView.animate().setDuration(shortAnimTime)
					.alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginFormView.setVisibility(show ? View.GONE
									: View.VISIBLE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	/**
	 * Represents an asynchronous login/registration task used to authenticate
	 * the user.
	 */
	public class CreateAccountTask extends AsyncTask<Void, Void, Boolean> {

		Intent returnIntent;

		@Override
		protected Boolean doInBackground(Void... params) {
			boolean success = true;

			returnIntent = new Intent();
			returnIntent.putExtra("nick", mUser);
			returnIntent.putExtra("tlf", mPhoneNumber);

			return success;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			mCreateAccount = null;
			showProgress(false);
			if (success) {
				setResult(RESULT_OK, returnIntent);
				finish();
			} else {
				// error?
			}
		}

		@Override
		protected void onCancelled() {
			mCreateAccount = null;
			showProgress(false);
		}
	}
}
