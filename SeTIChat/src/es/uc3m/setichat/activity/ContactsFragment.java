package es.uc3m.setichat.activity;

import java.util.ArrayList;
import java.util.UUID;

import model.entities.Connection;
import model.entities.Contact;
import model.entities.KeyRequest;
import model.entities.User;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListFragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.mobandme.ada.exceptions.AdaFrameworkException;

import es.uc3m.setichat.helpers.DatabaseHelper;
import es.uc3m.setichat.service.SeTIChatService;
import es.uc3m.setichat.utils.XMLParser;

/**
 * This activity will show the list of contacts. If a contact is clicked, a new
 * activity will be loaded with a conversation.
 * 
 * 
 * @author Guillermo Suarez de Tangil <guillermo.suarez.tangil@uc3m.es>
 * @author Jorge Blasco Al�s <jbalis@inf.uc3m.es>
 */
public class ContactsFragment extends ListFragment {
	// Service, that may be used to access chat features
	private SeTIChatService mService;

	private BroadcastReceiver ContactsReceiver, keyReceiver,
			revocationReceiver;
	private DatabaseHelper dbhelper;
	private User usuario;
	private int posicion_actual;

	private ArrayAdapter<Contact> contactsAdapter;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mService = ((MainActivitySeTIChat) activity).getService();
		try {
			dbhelper = new DatabaseHelper(getActivity());
			dbhelper.UserSet.fill();
			usuario = dbhelper.UserSet.get(0);
			dbhelper.contactSet.fill();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onStop() {
		super.onStop();
	}

	@Override
	public void onPause() {
		super.onPause();
		getActivity().getApplicationContext().unregisterReceiver(
				ContactsReceiver);
		getActivity().getApplicationContext().unregisterReceiver(keyReceiver);
	}

	@Override
	public void onResume() {
		super.onResume();
		revocationReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context arg0, Intent arg1) {
				dbhelper.UserSet.get(0);
				Toast.makeText(getActivity(),
						"recibida revocacion de clave de usuario",
						Toast.LENGTH_SHORT).show();
			}
		};
		IntentFilter revFilter = new IntentFilter();
		revFilter.addAction("es.uc3m.SeTIChat.KEY_REVOCATED");
		getActivity().getApplicationContext().registerReceiver(
				revocationReceiver, revFilter);

		keyReceiver = new BroadcastReceiver() {

			@Override
			public void onReceive(Context context, Intent intent) {
				// Si tiene clave publica, se inicia
				// conversacion segura, si no, no se inicia la conversacion
				if (intent.getIntExtra("code", 0) == 410) {
					Toast.makeText(getActivity(),
							"Clave publica no encontrada", Toast.LENGTH_SHORT)
							.show();
				} else if (intent.getIntExtra("code", 0) == 0) {
					Intent intent2 = new Intent(getActivity(),
							SeTIChatConversationActivity.class);
					intent2.putExtra("index", posicion_actual);
					intent2.putExtra("sourceID",
							dbhelper.contactSet.get(posicion_actual).tlf);
					intent2.putExtra("encrypted", true);
					startActivity(intent2);
				}
			}
		};

		IntentFilter keyFilter = new IntentFilter();
		keyFilter.addAction("es.uc3m.SeTIChat.KEY_RECEIVED");
		getActivity().getApplicationContext().registerReceiver(keyReceiver,
				keyFilter);

		ContactsReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				// releemos los contactos del usuario volviendo a llenar la
				// tabla
				try {
					dbhelper.contactSet.fill();
					dbhelper.UserSet.fill();
					usuario = dbhelper.UserSet.get(0);
					ArrayList<Contact> c = dbhelper.contactSet;
					contactsAdapter = new ArrayAdapter<Contact>(getActivity(),
							android.R.layout.simple_list_item_activated_1, c);
					setListAdapter(contactsAdapter);
					contactsAdapter.notifyDataSetChanged();

				} catch (AdaFrameworkException e) {
					e.printStackTrace();
				}

				model.entities.Header header = new model.entities.Header(
						usuario.token, "setichat@appspot.com", UUID
								.randomUUID().toString(), 5, false, false);
				model.entities.Connection connectmessage = new Connection(
						header, "");
				XMLParser parser = new XMLParser();
				mService = ((MainActivitySeTIChat) getActivity()).getService();
				mService.sendMessage(parser.generateMessage(header,
						connectmessage));

			}
		};
		IntentFilter contactsFilter = new IntentFilter();
		contactsFilter.addAction("es.uc3m.SeTIChat.CONTACTS_RESPONSE");
		getActivity().getApplicationContext().registerReceiver(
				ContactsReceiver, contactsFilter);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			dbhelper = new DatabaseHelper(getActivity());
			dbhelper.UserSet.fill();
			usuario = dbhelper.UserSet.get(0);
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		iniciar_convesacion(position);
	}

	private void iniciar_convesacion(int posicion_contacto) {
		posicion_actual = posicion_contacto;

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		builder.setMessage("�Qu� tipo de conversacion desea iniciar?")
				.setTitle("Seleccione tipo de conversacion");
		builder.setNegativeButton("NORMAL", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent intent = new Intent();
				intent.setClass(getActivity(),
						SeTIChatConversationActivity.class);
				intent.putExtra("index", posicion_actual);
				intent.putExtra("sourceID",
						dbhelper.contactSet.get(posicion_actual).tlf);
				intent.putExtra("encrypted", false);

				startActivity(intent);

			}
		});
		builder.setPositiveButton("SEGURA", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				try {
					dbhelper.contactSet.fill();
				} catch (AdaFrameworkException e) {
					e.printStackTrace();
				}
				Contact c = dbhelper.contactSet.get(posicion_actual);
				// Si el contacto no tiene clave publica o ha sido revocada, se
				// solicita
				if (c.isRevocated) {
					model.entities.Header header = new model.entities.Header(
							usuario.token, "setichat@appspot.com", UUID
									.randomUUID().toString(), 10, false, false);
					model.entities.KeyRequest krmessage = new KeyRequest(
							header, "public", dbhelper.contactSet
									.get(posicion_actual).tlf);
					XMLParser parser = new XMLParser();
					mService = ((MainActivitySeTIChat) getActivity())
							.getService();
					mService.sendMessage(parser.generateMessage(header,
							krmessage));
				} else {
					// Si tiene clave publica, se inicia
					// conversacion segura
					Intent intent = new Intent(getActivity(),
							SeTIChatConversationActivity.class);
					intent.putExtra("index", posicion_actual);
					intent.putExtra("sourceID",
							dbhelper.contactSet.get(posicion_actual).tlf);
					intent.putExtra("encrypted", true);

					startActivity(intent);
				}

			}
		});
		AlertDialog dialog = builder.create();
		dialog.show();
	}
}
