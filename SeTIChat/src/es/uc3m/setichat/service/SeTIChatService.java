package es.uc3m.setichat.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.UUID;

import model.entities.ChatMessage;
import model.entities.Contact;
import model.entities.ContactResponse;
import model.entities.Download;
import model.entities.Response;
import model.entities.Revocation;
import model.entities.User;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.mobandme.ada.Entity;
import com.mobandme.ada.exceptions.AdaFrameworkException;

import edu.gvsu.cis.masl.channelAPI.ChannelAPI;
import edu.gvsu.cis.masl.channelAPI.ChannelService;
import es.uc3m.setichat.R;
import es.uc3m.setichat.activity.MainActivitySeTIChat;
import es.uc3m.setichat.activity.SeTIChatConversationActivity;
import es.uc3m.setichat.helpers.DatabaseHelper;
import es.uc3m.setichat.helpers.MyCipher;
import es.uc3m.setichat.helpers.MySigner;
import es.uc3m.setichat.utils.XMLParser;

/**
 * This service is used to connect to the SeTIChat server. It should remain
 * running even if the app is not in the foreground
 * 
 * 
 * @author Guillermo Suarez de Tangil <guillermo.suarez.tangil@uc3m.es>
 * @author Jorge Blasco Al�s <jbalis@inf.uc3m.es>
 */

public class SeTIChatService extends Service implements ChannelService {
	String ERROR_401 = "Mensaje no conforme con la estructura de mensaje de SeTIChat";
	String ERROR_405 = "Tipo de mensaje no soportado";
	String ERROR_406 = "El	m�vil	del	usuario	no	se	corresponde	con	el	registrado	en	la	aplicaci�n";
	String ERROR_407 = "Usuario no registrado";
	String ERROR_408 = "Intento de chat con usuario no existente en SeTIChat";
	String ERROR_410 = "Clave	p�blica	no	encontrada";
	String ERROR_413 = "Error desconocido";

	String CODIGO_200 = "Mensaje de chat recibido por el servidor";
	String CODIGO_201 = "Usuario dado de alta correctamente. El cuerpo de este mensaje incluye un n�mero aleatorio a usar como sourceID en el resto de mensajes emitidos por el	cliente.";
	String CODIGO_202 = "Usuario conectado correctamente. Enviando mensajes pendiente...";
	String CODIGO_203 = "Nueva clave guardada. Enviando recovaciones a otros usuarios...";
	String CODIGO_204 = "Nueva clave guardada";

	public static final int FIRMADO_CORRECTO = 1;
	public static final int FIRMADO_INCORRECTO = -1;
	public static final int NO_FIRMADO = 0;

	// Database
	DatabaseHelper dbhelper;
	User usuario;
	Contact contacto_actual;

	// Used to communicate to the server
	ChannelAPI channel;

	// Used to bind activities
	private final SeTIChatServiceBinder binder = new SeTIChatServiceBinder();

	public SeTIChatService() {
		Log.i("SeTIChat Service", "Service constructor");
	}

	@Override
	public void onCreate() {
		super.onCreate();

		Log.i("SeTIChat Service", "Service created");
		// Inicializamos la BBDD y obtenemos el usuario
		try {
			dbhelper = new DatabaseHelper(getApplicationContext());

			dbhelper.UserSet.fill();
			usuario = dbhelper.UserSet.get(0);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// SeTIChat connection is set up in this step.
		// Mobile phone should be changed with the apropiate value
		channel = new ChannelAPI();
		String telefono = usuario.tlf;
		// Conectamos el servicio con el servidor
		this.connect(telefono);
		// Nos unimos al servicio
		binder.onCreate(this);

	}

	@Override
	public IBinder onBind(Intent intent) {
		Log.i("SeTIChat Service", "Service binded");
		return (binder);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.i("SeTIChat Service", "Service destrotyed");
		// When the service is destroyed, the connection is closed
		try {
			channel.close();
		} catch (Exception e) {
			System.out.println("Problem Closing Channel");
		}
		binder.onDestroy();
	}

	// Methods exposed to service binders
	// Login user, send message, update public key, etc.

	// All of them are implemented with AsyncTask examples to avoid UI Thread
	// blocks.

	// Primera conexion con el servidor, se envia solo el telefono
	// Ejecuci�n en segundo plano para no paralizar la ejecuci�n normal
	public void connect(String key) {
		final SeTIChatService current = this;
		class ChannelConnect extends AsyncTask<String, String, String> {

			protected String doInBackground(String... keys) {
				Log.i("Service connect", "Connect test");
				String key = keys[0];
				try {
					channel = new ChannelAPI("http://setichat.appspot.com",
							key, current); // Production
											// Example
					channel.open();

				} catch (Exception e) {
					System.out.println("Something went wrong...");
					Log.i("Service connect",
							"Error connecting..." + e.getLocalizedMessage());
				}
				return "ok";
			}

			protected void onProgressUpdate(String... progress) {
				// setProgressPercent(progress[0]);
			}

			protected void onPostExecute(String result) {
				//
			}
		}
		new ChannelConnect().execute(key, key, key);
	}

	// Envio de mensajes al servidor. Se realiza en segundo plano para no
	// paralizar el hilo de la app
	public void sendMessage(String message) {

		class SendMessage extends AsyncTask<String, String, String> {
			protected String doInBackground(String... messages) {
				Log.i("SendMessage", messages[0]);
				String message = messages[0];
				try {
					channel.send(message, "/chat");
				} catch (IOException e) {
					System.out.println("Problem Sending the Message");
				}
				return "ok";
			}

			protected void onProgressUpdate(String... progress) {
				// setProgressPercent(progress[0]);
			}

			protected void onPostExecute(String result) {

			}

		}
		new SendMessage().execute(message, message, message);
	}

	// Callback method for the Channel API. This methods are called by
	// ChannelService when some kind
	// of event happens

	/**
	 * Called when the client is able to correctly establish a connection to the
	 * server. In this case, the main activity is notified with a Broadcast
	 * Intent.
	 */
	@Override
	public void onOpen() {
		Log.i("onOpen", "Channel Opened");
		String intentKey = "es.uc3m.SeTIChat.CHAT_OPEN";
		Intent openIntent = new Intent(intentKey);
		// �Why should we set a Package?
		openIntent.setPackage("es.uc3m.setichat");
		Context context = getApplicationContext();
		context.sendBroadcast(openIntent);
	}

	/**
	 * Called when the client receives a chatMessage. In this case, the main
	 * activity is notified with a Broadcast Intent.
	 */
	@Override
	public void onMessage(String message) {
		String intentKey = "";
		String intentMessage = "";
		String intentContact = "";
		int intentCode = -1;
		Log.i("onMessage", "Message received :" + message);

		// parseo del mensaje
		PrintWriter out;
		try {
			out = new PrintWriter(Environment.getExternalStorageDirectory()
					+ "/setichat/response.xml");
			out.println(message);
			out.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}

		XMLParser parser = new XMLParser();
		message = message.replaceAll(">n ", ">");
		message = message.replaceAll(">n	", ">");
		message = message.replaceAll(">n<", "><");

		// gestionar mensaje segun tipo
		int type = parser.getTypeMessage(message);
		switch (type) {
		// CONTACT RESPONSE
		case 3:
			ContactResponse crep = parser.readContactResponse(message);

			try {
				dbhelper.contactSet.fill();
			} catch (AdaFrameworkException e2) {
				e2.printStackTrace();
			}
			for (Contact c : crep.contactList) {
				try {
					if (!dbhelper.isContactInDatabase(c.tlf)) {
						c.init();
						dbhelper.contactSet.add(c);
					}
				} catch (AdaFrameworkException e) {
					e.printStackTrace();
				}
			}
			try {
				dbhelper.contactSet.save();
				dbhelper.contactSet.fill();
				intentKey = "es.uc3m.SeTIChat.CONTACTS_RESPONSE";
				intentMessage = "Contactos recuperados";
			} catch (AdaFrameworkException e) {
				e.printStackTrace();
			}
			break;

		// MENSAJE DE CHAT
		case 4:

			Contact c = null;
			boolean goodSigned = true;
			boolean isSigned = false;

			ChatMessage chat = parser.readChatMessage(message);
			String idSource = chat.header.getIdSource();
			try {

				c = dbhelper.getContactbySourceId(idSource);
			} catch (AdaFrameworkException e2) {
				e2.printStackTrace();
			}
			// Log.i("MENSAJE_SINCOMPROBAR", chat.message);

			// si el mensaje esta cifrado, descifrarlo lo primero.
			if (chat.header.getEncrypted()) {
				MyCipher cifrador = new MyCipher();
				// Log.i("MENSAJE_CIFRADO", chat.message);
				try {
					dbhelper.UserSet.fill();
				} catch (AdaFrameworkException e) {
					e.printStackTrace();
				}
				usuario = dbhelper.UserSet.get(0);
				chat.message = cifrador.decrypt_concat_message_AES(
						chat.message, usuario.privateKey, usuario.publicKey);
				// Log.i("MENSAJE_SINCIFRAR", chat.message);
			}

			// si el mensaje esta firmado, verificar su firma.
			isSigned = chat.header.getSigned();
			if (isSigned) {
				MySigner firmador = new MySigner();
				goodSigned = firmador.check_signature(
						chat.header.getIdDestination(),
						chat.header.getIdMessage(), chat.message,
						chat.signature, c.publicKey);
			}
			if ((isSigned && goodSigned) | !isSigned) {
				try {
					chat.origin = chat.header.getIdSource();
					c.messages.add(chat);
					c.setStatus(Entity.STATUS_UPDATED);
					dbhelper.contactSet.save(c);
					dbhelper.contactSet.fill();
				} catch (AdaFrameworkException e1) {
					e1.printStackTrace();
				}

				Intent intent = new Intent(this, MainActivitySeTIChat.class);
				PendingIntent pIntent = PendingIntent.getActivity(this, 0,
						intent, 0);

				// NOTIFICACION DE MENSAJE
				Intent resultIntent = new Intent(this,
						SeTIChatConversationActivity.class);
				resultIntent.putExtra("sourceID", c.tlf);

				PendingIntent resultPendingIntent = PendingIntent.getActivity(
						this, 0, resultIntent,
						PendingIntent.FLAG_UPDATE_CURRENT);
				int mNotificationId = 001;

				NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
						this).setSmallIcon(R.drawable.setichat)
						.setContentTitle(c.nick).setContentText(chat.message)
						.setContentIntent(resultPendingIntent);
				NotificationManager mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
				mNotifyMgr.notify(mNotificationId, mBuilder.build());

			}
			intentKey = "es.uc3m.SeTIChat.MESSAGE_RECEIVED";
			intentMessage = chat.message;
			intentContact = chat.origin;

			if (isSigned && goodSigned) {
				intentCode = FIRMADO_CORRECTO;
			} else if (!isSigned) {
				intentCode = NO_FIRMADO;
			} else if (isSigned && !goodSigned) {
				intentCode = FIRMADO_INCORRECTO;
			}
			break;
		// GENERIC RESPONSE
		case 6:
			Response response = parser.readMessage(message);
			if (response.responseCode.equalsIgnoreCase("201")) {
				// RESPONSE: USER ACCOUNT CREATED SUCCESSFULLY
				// create account
				usuario.token = response.responseMessage;
				usuario.setStatus(com.mobandme.ada.Entity.STATUS_UPDATED);

				try {
					dbhelper.UserSet.save(usuario);

				} catch (AdaFrameworkException e) {
					e.printStackTrace();
				}
				// send our keys to server
				model.entities.Header header = new model.entities.Header(
						usuario.token, "setichat.appspot.com", UUID
								.randomUUID().toString(), 9, false, false);
				model.entities.Upload uploadmessage = new model.entities.Upload(
						header, usuario.publicKey, "public");
				this.sendMessage(parser.generateMessage(header, uploadmessage));

			} else if (response.responseCode.equalsIgnoreCase("203")) {
				intentKey = "es.uc3m.SeTIChat.CREATE_ACCOUNT";
				intentMessage = "Usuario creado correctamente, clave publica generada y subida al servidor";

			} else if (response.responseCode.equalsIgnoreCase("407")
					|| response.responseCode.equalsIgnoreCase("406")) {
				// RESPONSE: USER NOT CREATED
				intentKey = "es.uc3m.SeTIChat.CREATE_ACCOUNT";
				if (response.responseCode.equalsIgnoreCase("407")) {
					intentMessage = ERROR_407;
					Log.i("SeTIChat Service", ERROR_407);
					intentCode = 407;
				} else if (response.responseCode.equalsIgnoreCase("406")) {
					intentMessage = ERROR_406;
					Log.i("SeTIChat Service", ERROR_406);
					intentCode = 406;
				}
				// delete incomplete user
				usuario.setStatus(Entity.STATUS_DELETED);
				try {
					dbhelper.UserSet.save();
				} catch (AdaFrameworkException e) {
					e.printStackTrace();
				}
			} else if (response.responseCode.equalsIgnoreCase("200")
					|| response.responseCode.equalsIgnoreCase("408")) {
				intentKey = "es.uc3m.SeTIChat.MESSAGE_SENT";
				if (response.responseCode.equalsIgnoreCase("200")) {
					intentMessage = CODIGO_200;
					intentCode = 200;
				} else {
					intentMessage = ERROR_408;
					intentCode = 408;
				}
			} else if (response.responseCode.equalsIgnoreCase("401")
					|| response.responseCode.equalsIgnoreCase("405")
					|| response.responseCode.equalsIgnoreCase("413")) {
				intentKey = "es.uc3m.SeTIChat.CHAT_MESSAGE";
				if (response.responseCode.equalsIgnoreCase("401")) {
					intentMessage = ERROR_401;
					Log.i("SeTIChat Service", ERROR_401);
				} else if (response.responseCode.equalsIgnoreCase("405")) {
					intentMessage = ERROR_405;
					Log.i("SeTIChat Service", ERROR_405);
				} else if (response.responseCode.equalsIgnoreCase("413")) {
					intentMessage = ERROR_413;
					Log.i("SeTIChat Service", ERROR_413);
				}
			} else if (response.responseCode.equalsIgnoreCase("410")) {
				intentKey = "es.uc3m.SeTIChat.KEY_RECEIVED";
				intentCode = 410;
				intentMessage = ERROR_410;
				Log.i("SeTIChat Service", ERROR_410);
			}
			break;
		case 7:
			// REVOCATION MESSAGE
			Revocation revmessage = parser.readRevocation(message);
			try {
				contacto_actual = dbhelper
						.getContactbySourceId(revmessage.revocation);
				// borrado de su clave p�blica.
				contacto_actual.publicKey = "";
				contacto_actual.isRevocated = true;
				contacto_actual.setStatus(Entity.STATUS_UPDATED);
				dbhelper.contactSet.save(contacto_actual);
				dbhelper.contactSet.fill();
				intentKey = "es.uc3m.SeTIChat.KEY_REVOCATED";
			} catch (AdaFrameworkException e1) {
				e1.printStackTrace();
			}
			break;
		case 8:
			// KEY RESPONSE
			Download dwmessage = parser.readDownolad(message);
			try {
				dbhelper.contactSet.fill();
				contacto_actual = dbhelper
						.getContactbySourceId(dwmessage.mobile);
				// borrado de su clave p�blica.
				contacto_actual.publicKey = dwmessage.key;
				contacto_actual.isRevocated = false;
				contacto_actual.setStatus(Entity.STATUS_UPDATED);
				dbhelper.contactSet.save(contacto_actual);
				dbhelper.contactSet.fill();
			} catch (AdaFrameworkException e1) {
				e1.printStackTrace();
			}
			intentKey = "es.uc3m.SeTIChat.KEY_RECEIVED";
			intentCode = 0;
			break;
		// OTHER RESPONSE MESSAGES
		default:
			intentKey = "es.uc3m.SeTIChat.CHAT_MESSAGE";
			intentMessage = message;
			break;
		}
		// broadcast

		Intent msgIntent = new Intent(intentKey);
		msgIntent.putExtra("message", intentMessage);
		msgIntent.putExtra("code", intentCode);
		msgIntent.putExtra("contact", intentContact);
		msgIntent.setPackage("es.uc3m.setichat");
		Context context = getApplicationContext();
		context.sendBroadcast(msgIntent);
	}

	@Override
	public void onClose() {
		// Called when the connection is closed

	}

	@Override
	public void onError(Integer errorCode, String description) {
		// Called when there is an error in the connection

	}

}
