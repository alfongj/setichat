package model.entities;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

@Root(name = "message")
@Order(elements = { "header", "content" })
public class KeyRequest implements Content {
	@Element(name = "header")
	public Header header;

	@Path("content/keyrequest")
	@Element(name = "type")
	public String type;

	@Path("content/keyrequest")
	@Element(name = "mobile")
	public String mobile;

	public KeyRequest(Header header, String type, String mobile) {
		this.header = header;
		this.type = type;
		this.mobile = mobile;
	}

	public KeyRequest() {
		super();
	}
}
