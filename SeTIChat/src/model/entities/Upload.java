package model.entities;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

@Root(name = "message")
@Order(elements = { "header", "content" })
public class Upload implements Content {
	@Element(name = "header")
	public Header header;

	@Path("content/upload")
	@Element(name = "key")
	public String key;

	@Path("content/upload")
	@Element(name = "type")
	public String type;

	public Upload(Header header, String key, String type) {
		this.header = header;
		this.type = type;
		this.key = key;
	}

	public Upload() {
		super();
	}
}
