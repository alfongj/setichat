package model.entities;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

@Root(name = "message")
@Order(elements = { "header", "content" })
public class Revocation implements Content {

	@Element(name = "header")
	public Header header;

	@Path("content")
	@Element(name = "revokedMobile")
	public String revocation;

}
