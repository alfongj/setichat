package model.entities;

import com.mobandme.ada.Entity;
import com.mobandme.ada.annotations.Table;
import com.mobandme.ada.annotations.TableField;

@Table(name = "Contact")
public class ContactTlf extends Entity {
	@TableField(name = "tlf", datatype = DATATYPE_STRING)
	public String tlf;
}
