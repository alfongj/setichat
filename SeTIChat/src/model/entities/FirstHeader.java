package model.entities;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

@Root(name = "message", strict = false)
public class FirstHeader {

	@Path("header")
	@Element
	private String idSource;
	@Path("header")
	@Element
	public String idDestination;
	@Path("header")
	@Element
	private String idMessage;
	@Path("header")
	@Element
	private int type;
	@Path("header")
	@Element
	private boolean encrypted;
	@Path("header")
	@Element
	private boolean signed;

	public int getType() {
		return type;
	}

	public FirstHeader(String idSource, String idDestination, String idMessage,
			int type, boolean encrypted, boolean signed) {
		this.idSource = idSource;
		this.idDestination = idDestination;
		this.idMessage = idMessage;
		this.encrypted = encrypted;
		this.signed = signed;
		this.type = type;
	}

	public FirstHeader() {
		super();
	}

}
