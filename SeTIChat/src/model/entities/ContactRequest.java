package model.entities;

import java.util.ArrayList;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

@Root(name = "message")
@Order(elements = { "header", "content" })
public class ContactRequest implements Content {
	@Element
	public Header header;

	@Path("content")
	@ElementList(entry = "mobile")
	private ArrayList<String> mobileList;

	public ContactRequest() {
		super();
	}

	public ContactRequest(Header header, ArrayList<String> mobileList) {
		this.header = header;
		this.mobileList = mobileList;
	}

}
