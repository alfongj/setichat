package model.entities;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

@Root(name = "message")
@Order(elements = { "header", "content" })
public class Response implements Content {

	@Element
	public Header header;

	@Path("content/response")
	@Element
	public String responseCode;

	@Path("content/response")
	@Element
	public String responseMessage;

}
